<?php

use Illuminate\Database\Seeder;

class BroadcastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('broadcasters')->insert([
          'broadcaster'          => 'FoxKids',
        ]);
        DB::table('broadcasters')->insert([
          'broadcaster'          => 'Yorin',
        ]);
    }
}
