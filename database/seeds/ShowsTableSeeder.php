<?php

use Illuminate\Database\Seeder;

class ShowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $foxkids = ['Yu-Gi-Oh!','Metabots','Beyblade','Digimon','Pokemon'];
      $yorin = ['Dragon ball Z','Dragon ball GT','PowerPuffGirls'];

      foreach($foxkids as $show){
        DB::table('shows')->insert([
          'show'          => $show,
          'broadcaster'   => 'FoxKids'
        ]);
      }

      foreach($yorin as $show){
        DB::table('shows')->insert([
          'show'          => $show,
          'broadcaster'   => 'Yorin',
        ]);
      }
    }
}
