<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadcaster extends Model
{
    protected $table = 'broadcasters';

    public function shows(){
      return $this->hasMany('App\Show','broadcaster','broadcaster');
    }
}
