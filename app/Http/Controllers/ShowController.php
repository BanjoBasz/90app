<?php

namespace App\Http\Controllers;
use App\Show;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function index(){
      $shows = Show::all();
      return view('shows.index')->with('shows',$shows);
    }
    public function show($showName){
      return Show::where("show","=",$showName)->get()->first();
    }

    public function foxKids(){
      return Show::foxKids()->get();
    }

}
