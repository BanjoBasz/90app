<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $table = 'shows';
    protected $hidden = ['created_at','updated_at'];

    public function scopeFoxKids($query)
    {
      return $query->where('broadcaster', '=', 'FoxKids');
    }

    public function broadcaster(){
      return $this->hasOne('App\Broadcaster.php');
    }
}
